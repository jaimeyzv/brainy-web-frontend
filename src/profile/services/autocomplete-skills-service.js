import { inject, NewInstance } from 'aurelia-framework';
import { SkillClient } from '../../clients/skill-client';

@inject(NewInstance.of(SkillClient))
export class AutocompleteSkillsService {

  constructor(skillClient) {
    this.skillClient = skillClient;
  }

  searchSkills(query) {
    return this.skillClient.retrieveSkills(query);
  }
}
