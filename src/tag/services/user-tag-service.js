import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { UserTagClient } from '../../clients/user-tag-client';
import { constantsTagUserMessages } from '../../util/constant-user-tag-messages';

@inject(BaseComponent, UserTagClient)
export class UserTagService {

  constructor(baseComponent, userTagClient) {
    this.baseComponent = baseComponent;
    this.userTagClient = userTagClient;
    this.logger = LogManager.getLogger(UserTagService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  getMyTagsOnUser(email) {
    return this.userTagClient
        .getMyTagsOnUser(email)
        .then((tags) => {
          this.viewModel.setTagsOnUser(tags);
        });
  }

  tagUser(email, idTag) {
    return this.userTagClient
        .tagUser(email, idTag)
        .then((response) => {
          this.baseComponent.showMessageSuccess(
            constantsTagUserMessages.TAG_USER_MESSAGECODE[response.messageCode]);
          this.getMyTagsOnUser(email);
        })
        .catch((error) => {
          this.logger.error('Details of the error:', error);
          this.baseComponent.showMessageError();
        })
        .then(() => {
          this.baseComponent.dismissProgressHub();
        });
  }

  deleteTagOnUser(email, idTag) {
    this.baseComponent.showProgressHub();

    return this.userTagClient
      .deleteTagOnUser(email, idTag)
      .then(() => {
        this.baseComponent.showMessageSuccess('The tag was removed from the pal');
        this.getMyTagsOnUser(email);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  findUsersByTagName(tagName) {
    this.baseComponent.showProgressHub();

    this.userTagClient.findUsersByTagName(tagName)
      .then((users) => {
        this.viewModel.setUsers(users);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.viewModel.setUsers([]);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }
}
