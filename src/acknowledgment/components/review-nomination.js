import { bindable, inject } from 'aurelia-framework';
import { AcknowledgmentService } from '../../acknowledgment/services/acknowledgment-service';
import { constants } from '../../util/constants';
import { BaseComponent } from '../../util/base-component';
import Confirmation from '../../models/confirmation';
import { constantsMessage } from '../../util/constant-nomination-email-es';

@inject(AcknowledgmentService, BaseComponent)
export class ReviewNomination {
  @bindable nomination={};
  @bindable reload;
  isRejectable = false;
  isLocked = false;
  disabledValue = false;
  description = constantsMessage.NOMINATION_REJECTED_MESSAGE;

  constructor(acknowledgmentService, baseComponent) {
    this.acknowledgmentService = acknowledgmentService;
    this.baseComponent = baseComponent;
    this.confirmation = new Confirmation();
  }

  openRejectDescription() {
    this.isRejectable = true;
  }

  readRejectOnly(description) {
    this.isRejectable = true;
    this.description = description;
    this.isLocked = true;
    this.disabledValue = true;
  }

  clearReject() {
    this.isRejectable = false;
    this.isLocked = false;
    this.disabledValue = false;
    this.description = constantsMessage.NOMINATION_REJECTED_MESSAGE;
  }

  acceptNomination() {
    this.confirmation.status = constants.NOMINATION_STATUSES[2];
    this.confirmation.messageEmail = '';
    this.acceptRejectNomination();
  }

  rejectNomination() {
    this.isLocked = true;
    this.confirmation.status = constants.NOMINATION_STATUSES[1];
    this.confirmation.messageEmail = this.description;
    this.isLocked = true;
    this.acceptRejectNomination();
  }

  acceptRejectNomination() {
    this.isLocked = true;
    this.acknowledgmentService.acceptRejectNomination(this.nomination.id, this.confirmation)
    .then(() => {
      this.reload();
      this.closeReviewNominationModal();
      this.baseComponent.showMessageSuccess('Process has ended successfully.');
    })
    .catch((error) => {
      this.logger.error('Details of the error:', error);
      this.baseComponent.showMessageError();
    })
    .then(() => {
      this.baseComponent.dismissProgressHub();
    });
  }

  closeReviewNominationModal() {
    this.isLocked = false;
    this.isRejected = false;
    this.modal.close();
  }

  get isValidRejection() {
    return (this.description);
  }

  get disabledRejectNominationMessageSection() {
    return (!this.isValidRejection || this.isLocked);
  }
}
