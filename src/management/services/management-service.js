import { inject, LogManager } from 'aurelia-framework';
import { ManagementClient } from '../../clients/management-client';
import Parameter from '../../models/parameter';
import Headquarter from '../../models/headquarter';

@inject(ManagementClient)
export default class ManagementService {

  constructor(managementClient) {
    this.managementClient = managementClient;
    this.logger = LogManager.getLogger(ManagementService.name);
  }

  getParameters(headquarter) {
    return this.managementClient.getParameters(headquarter)
    .then(parameters => new Parameter(parameters));
  }

  getHeadquarters() {
    return this.managementClient.getHeadquarters()
    .then(headquarters => headquarters.map(h => new Headquarter(h)));
  }

  getAdministrators() {
    return this.managementClient.getAdministrators()
    .then(administrators => administrators);
  }

  getParametersByAdministrator(adminEmail) {
    return this.managementClient.getParametersByAdministrator(adminEmail)
    .then(headquarters => headquarters);
  }
}
