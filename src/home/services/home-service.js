import { inject, LogManager } from 'aurelia-framework';
import { ProfileClient } from '../../clients/profile-client';
import { BaseComponent } from '../../util/base-component';
import localStorageManager from '../../util/local-storage-manager';

@inject(BaseComponent, ProfileClient)
export class HomeService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(HomeService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  searchFromAPI(query) {
    this.baseComponent.showProgressHub();

    return this.profileClient.retrieveListOfProfiles(query)
      .then((users) => {
        const total = users.length;
        if (total > 0) {
          const message = `There were ${total} people that match your search`;
          this.markCurrentUser(users);
          this.baseComponent.showMessageSuccess(message);
        }
        this.viewModel.setResults(users);
      })
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  markCurrentUser(users) {
    const currentUser = users.find(user => (
      localStorageManager.isCurrentUser(user.email)
    ));

    if (currentUser) {
      currentUser.setCurrentUser(true);
    }
  }
}
