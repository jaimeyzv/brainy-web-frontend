import { bindable, inject } from 'aurelia-framework';
import { UserDetailService } from '../services/user-detail-service';
import { ProfileService } from '../../profile/services/profile-service';
import { BaseComponent } from '../../util/base-component';
import { constants } from '../../util/constant-profile-messages';

@inject(UserDetailService, ProfileService, BaseComponent)
export class UserDetail {

  @bindable() user;
  preloaderAllowed = false;
  progressValue = null;

  constructor(userDetailService, profileService, baseComponent) {
    this.profileService = profileService;
    this.api = userDetailService;
    this.api.setViewModel(this);
    this.baseComponent = baseComponent;
  }

  showSubDetail() {
    if (this.collapsed === undefined) {
      this.preloaderAllowed = true;
      this.collapsed = true;

      this.api
        .getMostEndorsedSkills(this.user.email)
        .then(() => {
          this.preloaderAllowed = false;
        });
    } else {
      this.collapsed = !this.collapsed;
    }
  }

  goToProfile() {
    const userName = this.user.email;
    this.profileService.getProfileInformation(userName).then((user) => {
      if (user.fechaFin !== null) {
        this.baseComponent.showMessageSuccess(constants.PROFILE_MESSAGECODE['profile.no.longer.available']);
      } else {
        this.api.navigateToProfile(this.user);
      }
    });
  }

  setSkills(skills) {
    this.user.skills = skills;
  }
}
