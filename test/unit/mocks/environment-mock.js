window.env = {
  OUTLOOK_ID: 'SOME-OUTLOOK-ID',
  BASE_SERVER_URL: 'https://the-brainy.avantica.net:8082',
  URL_REDIRECT: 'https://the-brainy.avantica.net:9000/',
};
